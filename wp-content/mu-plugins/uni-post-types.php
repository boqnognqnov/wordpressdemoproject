<?php
/**
 * Created by PhpStorm.
 * User: Boyan.Bozhidarov
 * Date: 29.3.2019 г.
 * Time: 15:50
 */

function university_post_types()
{


    // Slider item post type
    register_post_type('slider_item', [
        // This register the current post type in the default rest
        'show_in_rest' => false,
//        'supports' => ['title', 'editor', 'excerpt', 'custom-fields'],
        'supports' => ['title', 'editor', 'thumbnail'],
        'rewrite' => ['slug' => 'slider_items'],
        'has_archive' => false,
        'public' => true,
        'labels' => [
            'name' => 'Slider',
            'add_new_item' => 'Add new slider item',
            'edit_item' => 'Edit slider item',
            'all_items' => 'All slider items',
            'singular_name' => 'Slider items',
        ],
        'menu_icon' => 'dashicons-location-alt'
    ]);

    // Campus post type
    register_post_type('campus', [
        // This register the current post type in the default rest
        'show_in_rest' => true,
//        'supports' => ['title', 'editor', 'excerpt', 'custom-fields'],
        'supports' => ['title', 'editor', 'excerpt'],
        'rewrite' => ['slug' => 'campuses'],
        'has_archive' => true,
        'public' => true,
        'labels' => [
            'name' => 'Campuses',
            'add_new_item' => 'Add new campus',
            'edit_item' => 'Edit campus',
            'all_items' => 'All campuses',
            'singular_name' => 'Campuses',
        ],
        'menu_icon' => 'dashicons-location-alt'
    ]);

    // Event post type
    register_post_type('event', [
        // This register the current post type in the default rest
        'show_in_rest' => true,

        // Register this post type for Members plugin role. By default value is not 'event' is 'post'
        'capability_type' => 'event',
        'map_meta_cap' => true,

//        'supports' => ['title', 'editor', 'excerpt', 'custom-fields'],
        'supports' => ['title', 'editor', 'excerpt'],
        'rewrite' => ['slug' => 'events'],
        'has_archive' => true,
        'public' => true,
        'labels' => [
            'name' => 'Events',
            'add_new_item' => 'Add new event',
            'edit_item' => 'Edit event',
            'all_items' => 'All events',
            'singular_name' => 'Event',
        ],
        'menu_icon' => 'dashicons-calendar-alt'
    ]);

    // Program post type
    register_post_type('program', [
        // This register the current post type in the default rest
        'show_in_rest' => true,
//        'supports' => ['title', 'editor', 'excerpt', 'custom-fields'],
        'supports' => ['title', 'editor'],
        'rewrite' => ['slug' => 'programs'],
        'has_archive' => true,
        'public' => true,
        'labels' => [
            'name' => 'Programs',
            'add_new_item' => 'Add new program',
            'edit_item' => 'Edit program',
            'all_items' => 'All programs',
            'singular_name' => 'Program',
        ],
        'menu_icon' => 'dashicons-awards'
    ]);

    // Professors post type
    register_post_type('professor', [
//        'supports' => ['title', 'editor', 'excerpt', 'custom-fields'],
        // This register the current post type in the default rest
        'show_in_rest' => true,
        'supports' => ['title', 'editor', 'thumbnail'],
        'has_archive' => false,
        'public' => true,
        'labels' => [
            'name' => 'Professors',
            'add_new_item' => 'Add new Professor',
            'edit_item' => 'Edit Professor',
            'all_items' => 'All Professors',
            'singular_name' => 'Professor',
        ],
        'menu_icon' => 'dashicons-welcome-learn-more'
    ]);


    // Note post type
    register_post_type('note', [

        // Register this post type for Members plugin role. By default value is not 'event' is 'post'
        'capability_type' => 'note',
        'map_meta_cap' => true,

//        'supports' => ['title', 'editor', 'excerpt', 'custom-fields'],
        // This register the current post type in the default rest
        'show_in_rest' => true,
        'supports' => ['title', 'editor'],
        'has_archive' => false,
        'public' => false,
        'show_ui' => true,
        'labels' => [
            'name' => 'Notes',
            'add_new_item' => 'Add new note',
            'edit_item' => 'Edit Note',
            'all_items' => 'All Notes',
            'singular_name' => 'Notes',
        ],
        'menu_icon' => 'dashicons-welcome-write-blog'
    ]);


    // Like post type
    register_post_type('like', [

        // Register this post type for Members plugin role. By default value is not 'event' is 'post'
        'capability_type' => 'like',
        'map_meta_cap' => true,

//        'supports' => ['title', 'editor', 'excerpt', 'custom-fields'],
        // This register the current post type in the default rest
        'show_in_rest' => false,
        'supports' => ['title'],
        'has_archive' => false,
        'public' => false,
        'show_ui' => true,
        'labels' => [
            'name' => 'Likes',
            'add_new_item' => 'Add new like',
            'edit_item' => 'Edit Like',
            'all_items' => 'All Likes',
            'singular_name' => 'Likes',
        ],
        'menu_icon' => 'dashicons-heart'
    ]);
}

add_action('init', 'university_post_types');


function wptp_add_categories_to_attachments()
{
    register_taxonomy_for_object_type('category', 'attachment');
}

add_action('init', 'wptp_add_categories_to_attachments');


//// register new taxonomy which applies to attachments
//function wptp_add_location_taxonomy()
//{
//    $labels = array(
//        'name' => 'Image Destination',
//        'singular_name' => 'Image Destination',
//        'search_items' => 'Search image destinations',
//        'all_items' => 'All image destinations',
////        'parent_item' => 'Parent Location',
////        'parent_item_colon' => 'Parent Location:',
//        'edit_item' => 'Edit Image Destination',
//        'update_item' => 'Update Image Destination',
//        'add_new_item' => 'Add New Image Destination',
//        'new_item_name' => 'New Image Destination Name',
//        'menu_name' => 'Image Destination Location',
//    );
//
//    $args = array(
//        'labels' => $labels,
//        'hierarchical' => true,
//        'query_var' => 'true',
//        'rewrite' => 'true',
//        'show_admin_column' => 'true',
//    );
//
//    register_taxonomy('image_destination', 'attachment', $args);
//}
//
//add_action('init', 'wptp_add_location_taxonomy');
