<?php

get_header();
?>

<?php
while (have_posts()) {
    the_post();

    pageBanner([
        'title' => get_the_title(),
        'sub_title' => get_the_archive_description(),
    ]);

    ?>

    <div class="container container--narrow page-section">
        <h2>
            <?php the_title(); ?>
        </h2>

        <div class="metabox metabox--position-up metabox--with-home-link">
            <p>
                <a class="metabox__blog-home-link" href="<?= get_post_type_archive_link('program'); ?>">
                    <i class="fa fa-home"
                       aria-hidden="true"></i>
                    Program Home
                </a>
                <span class="metabox__main"><?php the_title(); ?></span>
            </p>
        </div>

        <div class="generic-content">
            <?php the_content(); ?>
        </div>
        <?php

        $professors = new WP_Query([
            'posts_per_page' => -1,
            'post_type' => 'professor',
            'orderBy' => 'title',
            'order' => 'ASC',
            'meta_query' => [
                [
                    'key' => 'related_programs',
                    'compare' => 'LIKE',
                    'value' => '"' . get_the_ID() . '"'
//                    'value' => get_the_ID()
                ]
            ]
        ]);
        ?>

        <?php if ($professors->have_posts()) { ?>

            <h2><?php echo get_the_title() . ' '; ?>Professors<br></h2>

            <ul class="professor-cards">
                <?php while ($professors->have_posts()) {
                    $professors->the_post();
                    ?>
                    <li class="professor-card__list-item">
                        <a class="professor-card" href="<?php the_permalink(); ?>">
                            <img class="professor-card__image"
                                 src="<?php the_post_thumbnail_url('professorPortrait'); ?>">
                            <span class="professor-card__name">
                               <?php the_title(); ?>
                        </span>
                        </a>
                    </li>
                <?php } ?>
            </ul>

        <?php } ?>

        <?php wp_reset_postdata(); ?>

        <?php
        //            $today = date('d/m/Y');
        $today = date('Ymd');
        $events = new WP_Query([
            'posts_per_page' => -1,
            'post_type' => 'event',
//                'orderBy' => 'post_date',
            'meta_key' => 'event_date',
            'orderBy' => 'meta_value_num',
            'order' => 'DESC',
            'meta_query' => [
                [
                    'key' => 'event_date',
                    'compare' => '>=',
                    'value' => $today,
                    'type' => 'numeric',
                ],
                [
                    'key' => 'related_programs',
                    'compare' => 'LIKE',
                    'value' => '"' . get_the_ID() . '"'
//                    'value' => get_the_ID()
                ]
            ]
        ]);
        ?>

        <?php if ($events->have_posts()) { ?>

            <h2>Upcoming<?php echo ' ' . get_the_title() . ' '; ?>Events<br></h2>

            <?php while ($events->have_posts()) {
                $events->the_post();

                get_template_part('template-parts/content','event');
            } ?>

        <?php } ?>

        <?php
        wp_reset_postdata();
        $relatedCampuses = get_field('related_campus');

        if ($relatedCampuses) {
            ?>
            <hr class="cection section-break">
            <h2><?php echo get_the_title() . ' '; ?>is available at these campuses:</h2>
            <ul class="min-list link-list">
                <?php foreach ($relatedCampuses as $relatedCampus) { ?>
                    <li>
                        <a href="<?php echo get_the_permalink($relatedCampus) ?>"><?php echo get_the_title($relatedCampus) ?></a>
                    </li>
                <?php } ?>
            </ul>
        <?php } ?>


        <br>
        <br>

        <div class="metabox">
            <p>Posted by <?php the_author_posts_link(); ?> on <?php the_time('d/m/y h:m'); ?>
                in <?php echo get_the_category_list(', '); ?></p>
        </div>
    </div>


<?php } ?>

<?php get_footer(); ?>


