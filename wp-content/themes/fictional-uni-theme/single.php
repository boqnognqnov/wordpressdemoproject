<?php

get_header();
?>


<?php
while (have_posts()) {
    the_post();

    pageBanner([
        'title' => get_the_title(),
        'sub_title' => get_the_archive_description(),
    ]);

    ?>


    <div class="container container--narrow page-section">
        <h2>
            <?php the_title(); ?>
        </h2>

        <div class="metabox metabox--position-up metabox--with-home-link">
            <p>
                <a class="metabox__blog-home-link" href="<?= site_url('/blog'); ?>">
                    <i class="fa fa-home"
                       aria-hidden="true"></i>
                    Blog Home
                </a>
                <span class="metabox__main"><?php the_title(); ?></span>
            </p>
        </div>

        <div class="generic-content">
            <?php the_content(); ?>
        </div>
        <br>
        <br>

        <div class="metabox">
            <p>Posted by <?php the_author_posts_link(); ?> on <?php the_time('d/m/y h:m'); ?>
                in <?php echo get_the_category_list(', '); ?></p>
        </div>
    </div>


<?php } ?>

<?php get_footer(); ?>


