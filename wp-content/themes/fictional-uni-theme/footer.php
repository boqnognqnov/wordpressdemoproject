<footer class="site-footer">

    <div class="site-footer__inner container container--narrow">

        <div class="group">

            <div class="site-footer__col-one">
                <h1 class="school-logo-text school-logo-text--alt-color"><a href="#"><strong>Fictional</strong>
                        University</a></h1>
                <p><a class="site-footer__link" href="#">555.555.5555</a></p>
            </div>

            <div class="site-footer__col-two-three-group">
                <div class="site-footer__col-two">

                    <?php if (pll_current_language() == 'en') { ?>
                        <h3 class="headline headline--small">Explore</h3>
                    <?php } ?>
                    <?php if (pll_current_language() == 'bg') { ?>
                        <h3 class="headline headline--small">Изследване</h3>
                    <?php } ?>
                    <nav class="nav-list">
                        <!--                        <ul>-->
                        <!--                            <li><a href="#">About Us</a></li>-->
                        <!--                            <li><a href="#">Programs</a></li>-->
                        <!--                            <li><a href="#">Events</a></li>-->
                        <!--                            <li><a href="#">Campuses</a></li>-->
                        <!--                        </ul>-->

                        <?php
                        //                        wp_nav_menu(['theme_location' => 'footer-menu-one']);
                        ?>

                        <ul>
                            <?php if (pll_current_language() == 'en') { ?>
                                <li <?php if (is_page('about-us') || wp_get_post_parent_id(0) == 11) echo 'class="current-menu-item"' ?> >
                                    <a href="<?= site_url('/en/about-us'); ?>">About Us</a>
                                </li>
                                <li <?php if (get_post_type() == 'campus') echo 'class="current-menu-item"' ?>>
                                    <a href="<?= site_url('en/campuses/'); ?>">Campuses</a>
                                </li>
                            <?php } ?>
                            <?php if (pll_current_language() == 'bg') { ?>
                                <li <?php if (is_page('за-нас/') || wp_get_post_parent_id(0) == 11) echo 'class="current-menu-item"' ?> >
                                    <a href="<?= site_url('/bg/за-нас/'); ?>">За Нас</a>
                                </li>
                                <li <?php if (get_post_type() == 'campus') echo 'class="current-menu-item"' ?>>
                                    <a href="<?= site_url('bg/campuses/'); ?>">Кампуси</a>
                                </li>
                            <?php } ?>
                        </ul>
                    </nav>
                </div>

                <div class="site-footer__col-three">

                    <?php if (pll_current_language() == 'en') { ?>
                        <h3 class="headline headline--small">Learn</h3>
                    <?php } ?>
                    <?php if (pll_current_language() == 'bg') { ?>
                        <h3 class="headline headline--small">Научи</h3>
                    <?php } ?>
                    <nav class="nav-list">
                        <!--                        <ul>-->
                        <!--                            <li><a href="#">Legal</a></li>-->
                        <!--                            <li><a href="-->
                        <? //= site_url('/privacy-policy-2'); ?><!--">Privacy</a></li>-->
                        <!--                            <li><a href="#">Careers</a></li>-->
                        <!--                        </ul>-->
                        <?php
                        //                        wp_nav_menu(['theme_location' => 'footer-menu-two']);
                        ?>

                        <?php if (pll_current_language() == 'en') { ?>
                            <li <?php if (get_post_type() == 'program') echo 'class="current-menu-item"' ?>>
                                <a href="<?= site_url('/en/programs'); ?>">Programs</a>
                            </li>
                            <li <?php if (get_post_type() == 'event') echo 'class="current-menu-item"' ?>>
                                <a href="<?= site_url('/en/events'); ?>">Events</a>
                            </li>
                        <?php } ?>
                        <?php if (pll_current_language() == 'bg') { ?>
                            <li <?php if (get_post_type() == 'program') echo 'class="current-menu-item"' ?>>
                                <a href="<?= site_url('/bg/programs/'); ?>">Програми</a>
                            </li>
                            <li <?php if (get_post_type() == 'event') echo 'class="current-menu-item"' ?>>
                                <a href="<?= site_url('/bg/events'); ?>">Събития</a>
                            </li>
                        <?php } ?>
                    </nav>
                </div>
            </div>

            <div class="site-footer__col-four">
                <h3 class="headline headline--small">Connect With Us</h3>
                <nav>
                    <ul class="min-list social-icons-list group">
                        <li><a href="#" class="social-color-facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </li>
                        <li><a href="#" class="social-color-twitter"><i class="fa fa-twitter"
                                                                        aria-hidden="true"></i></a></li>
                        <li><a href="#" class="social-color-youtube"><i class="fa fa-youtube"
                                                                        aria-hidden="true"></i></a></li>
                        <li><a href="#" class="social-color-linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </li>
                        <li><a href="#" class="social-color-instagram"><i class="fa fa-instagram"
                                                                          aria-hidden="true"></i></a></li>
                    </ul>
                </nav>
            </div>
        </div>

    </div>
</footer>

<!--<div class="search-overlay search-overlay--active">-->
<div class="search-overlay">
    <div class="search-overlay__top">
        <div class="container">
            <i class="fa fa-search search-overlay__icon" aria-hidden="true"></i>
            <input type="text" class="search-term" placeholder="What are you looking for ?" id="search-term">
            <i class="fa fa-window-close search-overlay__close" aria-hidden="true"></i>
        </div>
    </div>
    <div class="container">
        <div class="search-overlay__results" id="search-overlay__results">

        </div>
    </div>
</div>


<?php wp_footer(); ?>
</body>
</html>
