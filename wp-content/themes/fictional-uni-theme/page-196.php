<?php
get_header();
?>

<?php
//echo the_archive_description();
//pageBanner([
//    'title' => 'Добре дошли в нашия блог',
//    'sub_title' => 'Постове на Бългаски',
//]);
pageBanner([
    'title' => get_the_title(196),
    'sub_title' => get_field('page_banner_subtitle', 196),
]);

?>


<?php
$posts = new WP_Query([
    'post_type' => 'post',
    'posts_per_page' => -1,
    'category_name' => 'news',
]);
?>
<div class="container container--narrow page-section">
    <?php while ($posts->have_posts()) {
        $posts->the_post();
        ?>


        <div class="post-item">
            <h2 class="headline headline--medium headline--post-title"><a
                        href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <div class="metabox">
                <p>Posted by <?php the_author_posts_link(); ?> on <?php the_time('d/m/y h:m'); ?>
                    in <?php echo get_the_category_list(', '); ?></p>
            </div>
            <div class="generic-content">
                <?php the_excerpt(); ?>
                <p><a href="<?php the_permalink(); ?>">Continue reading &raquo;</a></p>
            </div>
        </div>
    <?php } ?>


    <?php echo paginate_links(); ?>
</div>


<?php
get_footer();
?>
