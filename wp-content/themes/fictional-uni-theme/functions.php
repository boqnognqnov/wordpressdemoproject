<?php


require get_theme_file_path('/common/search-route.php');
require get_theme_file_path('/common/like-route.php');
require get_theme_file_path('/common/template-header-img.php');


add_action('rest_api_init', 'uni_custom_rest');


function uni_custom_rest()
{
    register_rest_field('post', 'authorName', [
        'get_callback' => function () {
            return get_the_author();
        }
    ]);


    register_rest_field('note', 'userNoteCount', [
        'get_callback' => function () {
            return count_user_posts(get_current_user_id(), 'note');
        }
    ]);
}


function uneversity_files()
{
//    wp_enqueue_style('university_main_styles', get_stylesheet_uri());
    wp_enqueue_style('university_main_styles', get_stylesheet_uri(), null, microtime());
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
//    wp_enqueue_script('university_main_script', get_theme_file_uri('/js/scripts-bundled.js'), null, '1.0', true);
    wp_enqueue_script('university_main_script', get_theme_file_uri('/js/scripts-bundled.js'), null, microtime(), true);

    // If you want to load jQuery automatically
//    wp_enqueue_script('university_main_script', get_theme_file_uri('/js/scripts-bundled.js'), ['jquery'], microtime(), true);


//    wp_enqueue_script('googleMap', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyATAPRyIDzSOB2zoMySk50SIqSaMq3eRc8', null, microtime(), true);


    // Bind data to java script logic
    // Get the domain for global search REST logic
    wp_localize_script('university_main_script', 'backendJsData', [
        'root_url' => get_site_url(),
        'nonce' => wp_create_nonce('wp_rest'),
    ]);
}

add_action('wp_enqueue_scripts', 'uneversity_files');


function university_features()
{
    register_nav_menu('header-menu-location', 'Header Menu Location');
    register_nav_menu('footer-menu-one', 'Footer Menu One');
    register_nav_menu('footer-menu-two', 'Footer Menu Two');
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_image_size('professorLandscape', 400, 260, true);
    add_image_size('professorPortrait', 400, 260, true);
//    add_image_size('professorCropTest', 400, 260, ['left', 'top']);
    add_image_size('pageBanner', 1500, 350, true);
    add_image_size('slider-item', 1116, 400, true);
}

add_action('after_setup_theme', 'university_features');


function university_adjust_queries($query)
{
    // This will override the query logic for all posts
//    $query->set('posts_per_page', 1);

    if (!is_admin() && is_post_type_archive('program') && $query->is_main_query()) {
        $query->set('order_by', 'title');
        $query->set('order', 'ASC');
        $query->set('posts_per_page', -1);
    }


    if (!is_admin() && is_post_type_archive('campus') && $query->is_main_query()) {
        $query->set('posts_per_page', -1);
    }

    if (!is_admin() && is_post_type_archive('event') && $query->is_main_query()) {

        $today = date('Ymd');
//        $query->set('posts_per_page', 1);
        $query->set('meta_key', 'event_date');
        $query->set('order_by', 'meta_value_num');
        $query->set('order', 'ASC');
        $query->set('meta_query', [
            [
                'key' => 'event_date',
                'compare' => '>=',
                'value' => $today,
                'type' => 'numeric',
            ]
        ]);
    }

//    if (!is_admin() && is_post_type_archive('post') && $query->is_main_query()) {
//        $query->set('posts_per_page', -1);
//    }
}


add_action('pre_get_posts', 'university_adjust_queries');


//function universityMapKey($api)
//{
//    $api['key'] = 'AIzaSyATAPRyIDzSOB2zoMySk50SIqSaMq3eRc8';
//    return $api;
//}
//
//add_filter('acf/fields/google_map/api', 'universityMapKey');

function my_acf_google_map_api($api)
{

    $api['key'] = 'AIzaSyATAPRyIDzSOB2zoMySk50SIqSaMq3eRc8';

    return $api;

}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


function my_acf_init()
{

    acf_update_setting('google_api_key', 'AIzaSyATAPRyIDzSOB2zoMySk50SIqSaMq3eRc8');
}

add_action('acf/init', 'my_acf_init');

// Redirect subscribe accounts out of admin and onto homepage
add_action('admin_init', 'redirectSubsToFrontEnd');


function redirectSubsToFrontEnd()
{
    $currentUser = wp_get_current_user();
    if (count($currentUser->roles) === 1 && $currentUser->roles[0] === 'subscriber') {
        wp_redirect(site_url('/'));
        exit;
    }

}

add_action('admin_init', 'hide_editor');

function hide_editor()
{
    remove_post_type_support('slider_item', 'title');
    remove_post_type_support('slider_item', 'editor');
    remove_post_type_support('slider_item', 'page_banner_subtitle');
    remove_post_type_support('slider_item', 'page_banner_background_image');
}

add_action('wp_loaded', 'noSubsAdminBar');


function theme_add_post_type_support()
{

    add_post_type_support('page', 'excerpt');

    // Not working
//    add_post_type_support('page', 'page_banner_subtitle');
//    add_post_type_support('page', 'page_banner_background_image');

}

add_action('init', 'theme_add_post_type_support');

function noSubsAdminBar()
{
//    is_user_logged_in();
    $currentUser = wp_get_current_user();
    if (count($currentUser->roles) === 1 && $currentUser->roles[0] === 'subscriber') {
        show_admin_bar(false);
    }

}

// Customize the Login Screen
add_filter('login_headerurl', 'ourHeaderUrl');
function ourHeaderUrl()
{
    return esc_url(site_url('/'));
}

add_action('login_enqueue_scripts', 'loginCss');
function loginCss()
{
    wp_enqueue_style('university_main_styles', get_stylesheet_uri(), null, microtime());
}


add_filter('login_headertitle', 'changeLoginTitle');
function changeLoginTitle()
{
    return get_bloginfo('name');
}


// Force Note post to be private and sanitize NOTE post content !!!
add_filter('wp_insert_post_data', 'makeNotePrivate', 10, 2);
function makeNotePrivate($data, $postArr)
{

    if ($data['post_type'] == 'note') {

        // !$postArr['ID'] If the Note is new record
        if (count_user_posts(get_current_user_id(), 'note') > 4 && !$postArr['ID']) {
            die('You have reached your note limit !!!');
        }


        $data['post_title'] = sanitize_text_field($data['post_title']);
        $data['post_content'] = sanitize_textarea_field($data['post_content']);
    }

    if ($data['post_type'] == 'note' && $data['post_status'] != 'trash') {
        $data['post_status'] = 'private';
    }
    return $data;
}


//add_filter('pll_get_post_types', 'unset_cpt_pll', 10, 2);
//function unset_cpt_pll( $post_types, $is_settings ) {
//
//    $post_types['acf'] = 'acf';
//
//    return $post_types;
//}
//
//if( function_exists('acf_add_options_page') ) {
//    // Language Specific Options
//    // Translatable options specific languages. e.g., social profiles links
//    //
//
//    $languages = array( 'en', 'bg' );
//    foreach ( $languages as $lang ) {
//        acf_add_options_page( array(
//            'page_title' => 'Site Options (' . strtoupper( $lang ) . ')',
//            'menu_title' => __('Site Options (' . strtoupper( $lang ) . ')', 'text-domain'),
//            'menu_slug'  => "site-options-${lang}",
//            'post_id'    => $lang
//        ) );
//    }
//}










