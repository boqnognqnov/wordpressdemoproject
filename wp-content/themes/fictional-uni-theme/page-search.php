<?php
get_header();
?>

<?php

pageBanner([
    'title' => 'Search',
    'sub_title' => get_the_archive_description(),
]);
?>

<?php
get_search_form();
?>

<?php
get_footer();
?>
