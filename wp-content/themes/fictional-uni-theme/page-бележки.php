<?php

if (!is_user_logged_in()) {
    wp_redirect(esc_url(site_url('/')));
    exit;
}

get_header();
?>



<?php
//while (have_posts()) {
//    the_post();
//}
?>

<?php

pageBanner([
    'title' => 'Моите бележки',
    'sub_title' => get_the_archive_description(),
]);
?>

<div class="container container--narrow page-section">

    <div class="create-note">
        <h2 class="headline headline--medium">Създай нова бележка</h2>
        <input placeholder="Заглавие" class="new-note-title">
        <textarea placeholder="Твоята бележка" class="new-note-body"></textarea>
        <span class="submit-note">Създай</span>
        <span class="note-limit-message">Note limit reached !!!</span>
    </div>
    <ul class="min-list link-list" id="my-notes">
        <?php ?>
        <?php $userNotes = new WP_Query([
            'post_type' => 'note',
            'posts_per_page' => -1,
            'author' => get_current_user_id()
        ]); ?>
        <?php
        while ($userNotes->have_posts()) {
            $userNotes->the_post();
            ?>
            <li data-id="<?php the_ID(); ?>">
                <input readonly class="note-title-field"
                       value="<?php echo str_replace('Private: ', '', esc_attr(get_the_title())); ?>">
                <span class="edit-note"><i class="fa fa-pencil" aria-hidden="true"> Edit</i></span>
                <span class="delete-note"><i class="fa fa-trash-o" aria-hidden="true"> Delete</i></span>
                <textarea readonly class="note-body-field"><?php echo esc_attr(get_the_content()); ?></textarea>

                <!--                <span class="update-note btn btn-primary btn--small"><i class="fa fa-arrow-right"-->
                <!--                                                                     aria-hidden="true"> </i>Save</span>-->
                <span class="update-note btn-primary"><i class="fa fa-arrow-right"
                                                         aria-hidden="true"> </i>Save</span>
            </li>

        <?php } ?>
    </ul>
</div>


<?php
get_footer();
?>
