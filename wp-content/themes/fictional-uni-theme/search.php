<?php
get_header();
?>

<?php
//echo the_archive_description();
pageBanner([
    'title' => 'Search Results',
    'sub_title' => 'You search for: ' . get_search_query(),
]);
?>

<div class="container container--narrow page-section">
    <?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            get_template_part('template-parts/content', get_post_type());
        } ?>
        <?php echo paginate_links();
    } else {
        ?>
        <h2>No results match that search</h2>
    <?php }

    get_search_form();
    ?>

</div>


<?php
get_footer();
?>
