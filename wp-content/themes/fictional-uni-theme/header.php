<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php
//$current_url = rawurldecode($_SERVER['REQUEST_URI']);
//var_dump($current_url);
//die;
?>
<head>
    <meta charset="<?php bloginfo('charset') ?>" content="width=device-width, initial-scale=1"
    <meta name="viewport">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header class="site-header">
    <div class="container">
        <?php if (pll_current_language() == 'en') { ?>
            <h1 class="school-logo-text float-left">
                <a href="<?= site_url('/en/home'); ?>"><strong>Fictional</strong> University</a>
            </h1>
        <?php } ?>
        <?php if (pll_current_language() == 'bg') { ?>
            <h1 class="school-logo-text float-left">
                <a href="<?= site_url('/bg/начална-страница/'); ?>"><strong>Фективен</strong> Университет</a>
            </h1>
        <?php } ?>

        <a href="<?php echo esc_url(site_url('/search')); ?>" class="js-search-trigger site-header__search-trigger"><i
                    class="fa fa-search"
                    aria-hidden="true"></i></a>
        <i class="site-header__menu-trigger fa fa-bars" aria-hidden="true"></i>
        <div class="site-header__menu group">
            <nav class="main-navigation">

                <ul>
                    <?php if (pll_current_language() == 'en') { ?>
                        <li <?php if (is_page('home')) echo 'class="current-menu-item"' ?>>
                            <a href="<?= site_url('/en/home'); ?>">Home</a>
                        </li>
                        <li <?php if (get_post_type() == 'post') echo 'class="current-menu-item"' ?>>
                            <a href="<?= site_url('/en/blog'); ?>">Blog</a>
                        </li>

                        <li <?php if (get_post_type() == 'event') echo 'class="current-menu-item"' ?>>
                            <a href="<?= site_url('/en/events'); ?>">Events</a>
                        </li>
                        <li <?php if (get_post_type() == 'program') echo 'class="current-menu-item"' ?>>
                            <a href="<?= site_url('/en/programs'); ?>">Programs</a>
                        </li>
                        <li <?php if (is_page('about-us') || wp_get_post_parent_id(0) == 11) echo 'class="current-menu-item"' ?> >
                            <a href="<?= site_url('/en/about-us'); ?>">About Us</a>
                        </li>
                        <li <?php if (is_page('sample-page')) echo 'class="current-menu-item"' ?>>
                            <a href="<?= site_url('en/sample-page/'); ?>">Sample Page</a>
                        </li>
                        <li <?php if (get_post_type() == 'campus') echo 'class="current-menu-item"' ?>>
                            <a href="<?= site_url('en/campuses/'); ?>">Campuses</a>
                        </li>
                        <?php if (is_user_logged_in()) { ?>
                            <li <?php if (is_page('my-notes')) echo 'class="current-menu-item"' ?>>
                                <a href="<?= site_url('en/my-notes/'); ?>">My Notes</a>
                            </li>
                        <?php } ?>

                        <?php if (isset($tttttttttttt)) { ?>
                            <li <?php if (get_post_type() == 'program') echo 'class="current-menu-item"' ?>><a
                                        href="<?= get_post_type_archive_link('program'); ?>"></a></li>
                        <?php } ?>

                    <?php } ?>
                    <?php if (pll_current_language() == 'bg') { ?>
                        <li <?php if (is_page('home')) echo 'class="current-menu-item"' ?>>
                            <a href="<?= site_url('/bg/начална-страница/'); ?>">Начало</a>
                        </li>
                        <li <?php if (get_post_type() == 'post') echo 'class="current-menu-item"' ?>>
                            <!--                            <a href="--><? //= site_url('/bg/blog'); ?><!--">Блог</a>-->
                            <a href="<?= site_url('/bg/блог'); ?>">Блог</a>
                        </li>

                        <li <?php if (get_post_type() == 'event') echo 'class="current-menu-item"' ?>>
                            <a href="<?= site_url('/bg/events'); ?>">Събития</a>
                        </li>
                        <li <?php if (get_post_type() == 'program') echo 'class="current-menu-item"' ?>>
                            <a href="<?= site_url('/bg/programs/'); ?>">Програми</a>
                        </li>
                        <li <?php if (is_page('за-нас/') || wp_get_post_parent_id(0) == 11) echo 'class="current-menu-item"' ?> >
                            <a href="<?= site_url('/bg/за-нас/'); ?>">За Нас</a>
                        </li>
                        <li <?php if (is_page('sample-page')) echo 'class="current-menu-item"' ?>>
                            <a href="<?= site_url('bg/демо-страница/'); ?>">Демо Страница</a>
                        </li>
                        <li <?php if (get_post_type() == 'campus') echo 'class="current-menu-item"' ?>>
                            <a href="<?= site_url('bg/campuses/'); ?>">Кампуси</a>
                        </li>
                        <?php if (is_user_logged_in()) { ?>
                            <li <?php if (is_page('my-notes')) echo 'class="current-menu-item"' ?>>
                                <a href="<?= site_url('bg/бележки/ '); ?>">Бележки</a>
                            </li>
                        <?php } ?>
                    <?php } ?>




                    <?php

                    require get_theme_file_path('/common/get-route-path.php');


                    $translations = pll_the_languages(array('raw' => 1));

                    foreach ($translations as $key => $value) {
//                    $output .= '<li><a href="'.$value['url'].'?id='.$id.'&nav='.$nav.'"><img src="'.$value['flag'].'" alt="'.$value['slug'].'"> ' .$value['name'].'</a></li>'
//                        var_dump($key);
//                        var_dump($value);

                        ?>
                        <li>
                            <a href="<?=
                            defineNewRoute($value['slug']);
                            ?>"><img
                                        src="<?php echo get_theme_file_uri('/images/flags/' . $value['slug'] . '.png'); ?>"
                                        width="70%"></a>
                        </li>

                    <?php } ?>
                </ul>
                <!--                <ul class="head-lang-switcher">--><?php //pll_the_languages(); ?><!--</ul>-->

                <?php
                //                die;
                //                                                wp_nav_menu(['theme_location' => 'header-menu-location']);
                ?>
            </nav>
            <div class="site-header__util">
                <?php if (!is_user_logged_in()) { ?>
                    <a href="<?php echo esc_url(site_url('/wp-login.php')); ?>"
                       class="btn btn--small btn--orange float-left push-right">Login</a>
                    <a href="<?php echo esc_url(site_url('/wp-signup.php')); ?>"
                       class="btn btn--small  btn--dark-orange float-left">Sign Up</a>
                <?php } ?>
                <?php if (is_user_logged_in()) { ?>
                    <a href="<?php echo wp_logout_url(); ?>"
                       class="btn btn--small  btn--dark-orange float-left btn--with-photo">
                        <span class="site-header__avatar"><?php echo get_avatar(get_current_user_id(), 60); ?></span>
                        <span class="btn__text">Log Out</span>
                    </a>
                <?php } ?>
                <a href="<?php echo esc_url(site_url('/search')); ?>" class="search-trigger js-search-trigger"><i
                            class="fa fa-search" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</header>


