<?php


add_action('rest_api_init', 'initLikeRoutes');

function initLikeRoutes()
{
    register_rest_route('university/v1', 'manage/like', [
        'methods' => WP_REST_Server::CREATABLE,
        'callback' => 'createLike'
    ]);

    register_rest_route('university/v1', 'manage/like', [
        'methods' => WP_REST_Server::DELETABLE,
        'callback' => 'deleteLike'
    ]);
}

function createLike($parameters)
{

//    if (current_user_can('publish_note')) {
    if (!is_user_logged_in()) {
        die('Only login user can create a like');
    }

    $professorId = sanitize_text_field($parameters['professorId']);


    $userAlreadyLiked = new WP_Query(
        [
            'author' => get_current_user_id(),
            'post_type' => 'like',
            'meta_query' => [
                [
                    'key' => 'liked_professor_id',
                    'compare' => '=',
                    'value' => $professorId
                ]
            ],
        ]
    );

    $element = [
        'post_type' => 'like',
        'post_status' => 'publish',
        'post_title' => 'Like Test Title',
//        'post_content' => 'Like Test Content',
        'meta_input' => [
            // Custom field name
            'liked_professor_id' => $professorId
        ],
    ];

    if ($userAlreadyLiked->have_posts() == 0 && get_post_type($professorId) == 'professor') {

        $element['id'] = wp_insert_post($element);
        return $element;
    } else {
        die('Invalid professor ID');
    }


}

function deleteLike($parameters)
{
    $likePostId = sanitize_text_field($parameters['likePostId']);

    if (get_current_user_id() == (int)get_post_field('post_author', $likePostId) && get_post_type($likePostId) == 'like') {
        wp_delete_post($likePostId, true);
        return ['status' => true];
    } else {
        die('You do not have permission to do that');
    }
}