<?php


add_action('rest_api_init', 'uniRegisterSearchRoutes');

function uniRegisterSearchRoutes()
{
    register_rest_route('university/v1', 'search', [
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'universitySearchResults'
    ]);
}


function universitySearchResults($parameters)
{
    $rawResults = new WP_Query([
        'post_type' => ['professor', 'post', 'page', 'event', 'campus', 'program'],
        's' => sanitize_text_field($parameters['term'])
    ]);

    $results = [];

    while ($rawResults->have_posts()) {
        $rawResults->the_post();

        $item['id'] = get_the_ID();
        $item['title'] = get_the_title();
        $item['permalink'] = get_the_permalink();
        $item['authorName'] = get_the_author();
        if (get_post_type() === 'professor') {
            $item['image'] = get_the_post_thumbnail_url(0, 'professorPortrait');
        }

        if (get_post_type() === 'program') {
            $relatedCampuses = get_field('related_campus');
            if ($relatedCampuses) {
                foreach ($relatedCampuses as $campus2) {
                    $item2['id'] = 0;
                    $item2['title'] = get_the_title($campus2);
                    $item2['permalink'] = get_the_permalink($campus2);
                    $results['campus'][] = $item2;
                }
            }
        }

        $results[get_post_type()][] = $item;
        $item = [];
    }


    if (!empty($results)) {
        // Get professors by related programs
        $programFilters = [];
        foreach ($results as $result) {
            foreach ($result as $item) {
                $programFilters[] = [
                    'key' => 'related_programs',
                    'compare' => 'LIKE',
                    'value' => '"' . $item['id'] . '"'
                ];
            }
        }


        $programRelationshipQuery = new WP_Query([
            'post_type' => ['professor', 'event'],
            'meta_query' => $programFilters
        ]);


//        return $programRelationshipQuery->posts;

        while ($programRelationshipQuery->have_posts()) {
            $programRelationshipQuery->the_post();

            if (get_post_type() === 'professor') {
                $item['id'] = get_the_ID();
                $item['title'] = get_the_title();
                $item['permalink'] = get_the_permalink();
                $item['authorName'] = get_the_author();
                $item['image'] = get_the_post_thumbnail_url(0, 'professorPortrait');
            }

            if (get_post_type() === 'event') {
                $eventDate = \DateTime::createFromFormat('d/m/Y', get_field('event_date'));
                $description = null;
                if (has_excerpt()) {
                    $description = get_the_excerpt();
                } else {
                    $description = wp_trim_words(get_the_content(), 18);
                }

                $item['id'] = get_the_ID();
                $item['title'] = get_the_title();
                $item['permalink'] = get_the_permalink();
                $item['month'] = $eventDate->format('M');
                $item['day'] = $eventDate->format('d');
                $item['description'] = $description;

            }

            $results[get_post_type()][] = $item;
            $item = [];
        }

        $results[get_post_type()] = array_values(array_unique($results[get_post_type()], SORT_REGULAR));
    }


    return $results;
}
