<?php

function defineNewRoute($langCode)
{


    $newPath = "/en/home";
    if (pll_current_language() == 'en' && $langCode != 'en') {

        if (checkTheRoute('/en/home/')) {
            $newPath = '/bg/начална-страница';
        }

        if (checkTheRoute('/en/blog/')) {
//            $newPath = '/bg/blog/';
            $newPath = '/bg/блог/';
        }

        if (checkTheRoute('/en/events')) {
            $newPath = '/bg/events';
        }

        if (checkTheRoute('/en/programs')) {
            $newPath = '/bg/programs';
        }

        if (checkTheRoute('/en/about-us/')) {
            $newPath = '/bg/за-нас';
        }

        if (checkTheRoute('/en/sample-page/')) {
            $newPath = '/bg/демо-страница/';
        }

        if (checkTheRoute('/en/campuses/')) {
            $newPath = '/bg/campuses/';
        }

        if (checkTheRoute('/en/my-notes/')) {
            $newPath = '/bg/бележки/';
        }

    } elseif (pll_current_language() == 'bg' && $langCode != 'bg') {

        if (checkTheRoute('/bg/начална-страница')) {
            $newPath = '/en/home';
        }

//        if (checkTheRoute('/bg/blog/')) {
        if (checkTheRoute('/bg/блог/')) {
            $newPath = '/en/blog/';
        }

        if (checkTheRoute('/bg/events')) {
            $newPath = '/en/events';
        }

        if (checkTheRoute('/bg/programs')) {
            $newPath = '/en/programs';
        }

        if (checkTheRoute('/bg/за-нас/')) {
            $newPath = '/en/about-us';
        }

        if (checkTheRoute('/bg/демо-страница/')) {
            $newPath = '/en/sample-page/';
        }

        if (checkTheRoute('/bg/campuses/')) {
            $newPath = '/en/campuses/';
        }

        if (checkTheRoute('/bg/бележки/')) {
            $newPath = '/en/my-notes/';
        }

    } elseif (pll_current_language() == $langCode) {
        $newPath = get_permalink();
    }


    return $newPath;
}

function checkTheRoute($routeForCheck = '')
{
    $current_url = rawurldecode($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

    if (strpos($current_url, $routeForCheck) !== false) {
        return true;
    } else {
        return false;
    }
}
