<?php

function pageBanner($args = [])
{
    ?>
    <?php $pageBannerImage = get_field('page_banner_background_image'); ?>

    <?php if (isset($ttttttttttttttttt)) { ?>
    <div class="page-banner__bg-image"
         style="background-image: url(<?= get_theme_file_uri('/images/ocean.jpg') ?>);"></div>
<?php } ?>

    <?php if (isset($ttttttttttttttttt)) { ?>
    <div class="page-banner__bg-image"
         style="background-image: url(<?php echo $pageBannerImage['url'] ?>);"></div>
<?php } ?>


    <?php
    $prefer_assets_source = false;
    if (isset($args['background_img_assets']['default'])) {
        $prefer_assets_source = $args['background_img_assets']['default'];
    }


    if ($pageBannerImage && $prefer_assets_source == false) {
        $pageBannerImage = $pageBannerImage['sizes']['pageBanner'];
    } elseif (isset($args['background_img_assets'])) {
        $pageBannerImage = get_theme_file_uri('/images/' . $args['background_img_assets']['url']);
    } else {
        $pageBannerImage = get_theme_file_uri('/images/ocean.jpg');
    }
    ?>


    <?php
//    the_title()''
    $title = get_the_title();
    if (isset($args['title'])) {
        $title = $args['title'];
    }
    ?>

    <?php
//    $title=get_the_field('page_banner_subtitle');
    $subTitle = get_field('page_banner_subtitle');
    if (isset($args['sub_title'])) {
        $subTitle = $args['sub_title'];
    }
    ?>


    <div class="page-banner">
        <div class="page-banner__bg-image"
             style="background-image: url(<?php echo $pageBannerImage ?>);"></div>


        <div class="page-banner__content container container--narrow">
            <h1 class="page-banner__title"><?php echo $title; ?></h1>
            <div class="page-banner__intro">
                <p><?php echo $subTitle; ?></p>
            </div>
        </div>
    </div>
    <?php
}