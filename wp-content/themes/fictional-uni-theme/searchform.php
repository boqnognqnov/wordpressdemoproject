<br>
<br>
<br>
<div class="row">
    <div class="container">
        <div class="generic-content">
            <form method="GET" action="<?php echo esc_url(site_url('/')); ?>">
                <label for="s">Perform a New Search</label>
                <p>&nbsp;</p>
                <input id="s" type="search" name="s">
                <input type="submit" value="Search">
            </form>
        </div>
    </div>
</div>