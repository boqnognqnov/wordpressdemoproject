<?php
get_header();
?>



<?php
//echo the_archive_description();
//pageBanner([
//    'title' => 'Search Results',
//    'sub_title' => 'You search for: ' . get_search_query(),
//]);

$pageBannerImage = get_field('page_banner_background_image');
if ($pageBannerImage) {
    $pageBannerImage = $pageBannerImage['sizes']['pageBanner'];
} else {
    $pageBannerImage = get_theme_file_uri('/images/ocean.jpg');
}

$title = get_the_title();
$subTitle = get_field('page_banner_subtitle');
?>

<div class="page-banner">
    <div class="page-banner__bg-image"
         style="background-image: url(<?php echo $pageBannerImage ?>);"></div>
    <div class="page-banner__content container t-center c-white">
        <h1 class="headline headline--large"><?php echo $title; ?></h1>
        <h2 class="headline headline--medium">We think you&rsquo;ll like it here.</h2>
        <h3 class="headline headline--small">Why don&rsquo;t you check out the <strong>major</strong> you&rsquo;re
            interested in?</h3>
        <div class="page-banner__intro">
            <p><?php echo $subTitle; ?></p>
        </div>
        <a href="<?php echo get_post_type_archive_link('program') ?>" class="btn btn--large btn--blue">Find Your
            Major</a>
    </div>
</div>

<div class="full-width-split group">
    <div class="full-width-split__one">
        <div class="full-width-split__inner">
            <h2 class="headline headline--small-plus t-center">Upcoming Events</h2>

            <?php
            //            $today = date('d/m/Y');
            $today = date('Ymd');
            $events = new WP_Query([
                'posts_per_page' => -1,
                'post_type' => 'event',
//                'orderBy' => 'post_date',
                'meta_key' => 'event_date',
                'orderBy' => 'meta_value_num',
                'order' => 'DESC',
                'meta_query' => [
                    [
                        'key' => 'event_date',
                        'compare' => '>=',
                        'value' => $today,
                        'type' => 'numeric',
                    ]
                ]
            ]);
            ?>
            <?php while ($events->have_posts()) {
                $events->the_post();
                get_template_part('template-parts/content', 'event');
            } ?>
            <?php wp_reset_postdata(); ?>


            <p class="t-center no-margin"><a href="<?php echo get_post_type_archive_link('event'); ?>"
                                             class="btn btn--blue">View
                    All Events</a></p>

        </div>
    </div>
    <div class="full-width-split__two">
        <div class="full-width-split__inner">
            <h2 class="headline headline--small-plus t-center">From Our Blogs</h2>

            <?php
            $homePagePosts = new WP_Query([
                'post_type' => 'post',
                'posts_per_page' => 2,
                'category_name' => 'news',
            ]);

            while ($homePagePosts->have_posts()) {
                $homePagePosts->the_post(); ?>


                <div class="event-summary">
                    <a class="event-summary__date event-summary__date--beige t-center" href="<?php the_permalink(); ?>">
                        <span class="event-summary__month"><?php the_time('M'); ?></span>
                        <span class="event-summary__day"><?php the_time('d'); ?></span>
                    </a>
                    <div class="event-summary__content">
                        <h5 class="event-summary__title headline headline--tiny"><a
                                    href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </h5>
                        <p>
                            <?php if (has_excerpt()) {
                                echo get_the_excerpt();
                            } else {
                                echo wp_trim_words(get_the_content(), 18);
                            } ?>
                            <a href="<?php the_permalink(); ?>"
                               class="nu gray">Read more</a>
                        </p>
                    </div>
                </div>

            <?php } ?>
            <?php wp_reset_postdata(); ?>


            <p class="t-center no-margin"><a href="<?php echo site_url('/blog'); ?>" class="btn btn--yellow">View All
                    Blog Posts</a></p>
        </div>
    </div>
</div>


<?php

$sliderItems = new WP_Query([
    'posts_per_page' => -1,
    'post_type' => 'slider_item'
]);
?>
<div class="hero-slider">
    <?php if ($sliderItems->have_posts()) { ?>
        <?php while ($sliderItems->have_posts()) {
            $sliderItems->the_post();
            ?>
            <div class="hero-slider__slide"
                 style="background-image: url(<?php the_post_thumbnail_url('slider-item'); ?>);">
                <div class="hero-slider__interior container">
                    <div class="hero-slider__overlay">
                        <h2 class="headline headline--medium t-center">Free Transportation</h2>
                        <p class="t-center">All students have free unlimited bus fare.</p>
                        <p class="t-center no-margin"><a href="#" class="btn btn--blue">Learn more</a></p>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>

<?php
get_footer();
?>
