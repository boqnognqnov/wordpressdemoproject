import $ from 'jquery';

class GlobalSearch {
    constructor() {
        this.resultsDiv = $('#search-overlay__results');

        this.openBtn = $('.js-search-trigger');
        this.closeBtn = $('.search-overlay__close');
        this.searchOverlay = $('.search-overlay');

        this.searchField = $('#search-term');

        this.isSpinnerVisible = false;

        this.events();
        this.isOverlayOpen = false;
        this.previousSearchVal = '';
        this.typingTimer = null;
    }

    // Events
    events() {
        this.openBtn.on("click", this.openOverlay.bind(this));
        this.closeBtn.on("click", this.closeOverlay.bind(this));

        $(document).on('keydown', this.keyPressDispatcher.bind(this));
        this.searchField.on('keyup', this.searchTyping.bind(this));
    }

    openOverlay(event) {
        if (typeof event !== 'undefined') {
            event.preventDefault();
        }

        this.searchOverlay.addClass('search-overlay--active');
        $('body').addClass('body-no-scroll');
        this.searchField.val('');
        setTimeout(() => {
            this.searchField.focus()
        }, 301);

        this.isOverlayOpen = true;
        return false;
    }

    closeOverlay() {
        this.searchOverlay.removeClass('search-overlay--active');
        $('body').removeClass('body-no-scroll');
        this.isOverlayOpen = false;
    }

    searchTyping(event) {
        // Skip shift, ctrl or other special keys
        if (this.searchField.val() !== this.previousSearchVal) {

            clearTimeout(this.typingTimer);

            if (this.searchField.val().trim(' ').length > 0) {
                if (this.isSpinnerVisible === false) {
                    this.resultsDiv.html('<div class="spinner-loader"></div>');

                    this.isSpinnerVisible = true;
                }

                this.typingTimer = setTimeout(this.getResults.bind(this, event), 750);

            } else {
                this.resultsDiv.html('');
                this.isSpinnerVisible = false;
            }


        }

        this.previousSearchVal = this.searchField.val();
    }

    getResults(event) {
        console.log(event);


        // $.when(
        //     $.getJSON(backendJsData.root_url + '/wp-json/wp/v2/posts?search=' + this.searchField.val()),
        //     $.getJSON(backendJsData.root_url + '/wp-json/wp/v2/pages?search=' + this.searchField.val())
        // ).then((posts, pages) => {
        //     var combineResults = posts[0].concat(pages[0]);
        //
        //     var str = '<h2>General Information</h2>';
        //     if (combineResults.length > 0) {
        //         str += '<ul class="link-list min-list">';
        //         for (var i = 0; combineResults[i]; i++) {
        //             var author = '';
        //             if (combineResults[i].type === 'post') {
        //                 author = 'by ' + combineResults[i].authorName;
        //             }
        //             str += '<li><a href="' + combineResults[i].link + '">' + combineResults[i].title.rendered + '</a> ' + author + '</li>';
        //         }
        //
        //         str += '</ul>';
        //     } else {
        //         str += '<p>No results !!!</p>';
        //     }
        //
        //
        //     this.resultsDiv.html(str);
        // }, (error) => {
        //     this.resultsDiv.html('<p>Unexpected Error !!!</p>');
        // });


        $.when(
            $.getJSON(backendJsData.root_url + '/wp-json/university/v1/search?term=' + this.searchField.val())
        ).then((results) => {
            var str = '<h2>General Information</h2>';
            console.log(results);

            for (var category in results) {
                if (results.hasOwnProperty(category)) {

                    str += '<p>' + category + '</p>';

                    var itemsInCategory = results[category];
                    for (var itemKey in itemsInCategory) {
                        if (itemsInCategory.hasOwnProperty(itemKey)) {


                            str += '<ul class="link-list min-list">';
                            var author = '';
                            // if (category === 'post' || category === 'professor' || category === 'page' || category === 'event') {
                            if (category === 'post' || category === 'event') {
                                author = 'by ' + itemsInCategory[itemKey].authorName;
                            }

                            if (category === 'professor') {
                                str += '<li>' +
                                    '<a class="professor-card" href="' + itemsInCategory[itemKey].permalink + '">' +
                                    '<img class="professor-card__image" src="' + itemsInCategory[itemKey].image + '">' +
                                    '<span class="professor-card__name">' + itemsInCategory[itemKey].title + '</span>' +
                                    '</a> ' + author +
                                    '</li>';
                            } else {
                                str += '<li><a href="' + itemsInCategory[itemKey].permalink + '">' + itemsInCategory[itemKey].title + '</a> ' + author + '</li>';
                            }

                            str += '</ul>';
                        }
                    }
                }
            }

            this.resultsDiv.html(str);
        }, (error) => {
            this.resultsDiv.html('<p>Unexpected Error !!!</p>');
        });


        this.isSpinnerVisible = false;
    }

    keyPressDispatcher(event) {

        console.log(event.keyCode);

        var selectedElementType = document.activeElement.tagName;
        selectedElementType = selectedElementType.toLowerCase();
        console.log(selectedElementType);

        // Press 's'
        // if (event.keyCode === 83 && this.isOverlayOpen === false && !$('input', 'textarea').is(':focus')) {
        if (event.keyCode === 83 && this.isOverlayOpen === false && selectedElementType !== 'input' && selectedElementType !== 'textarea') {
            this.openOverlay();
        }

        // Press ESC
        if (event.keyCode === 27 && this.isOverlayOpen === true) {
            this.closeOverlay();
        }
    }
}

export default GlobalSearch;
