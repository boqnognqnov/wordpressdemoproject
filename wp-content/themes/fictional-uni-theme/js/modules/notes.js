import $ from 'jquery';

class Notes {
    constructor() {
        this.events();
    }

    events() {
        $('.delete-note').on('click', this.deleteNote);
        $('.edit-note').on('click', this.editNote.bind(this));
        $('.update-note').on('click', this.updateNote.bind(this));
        $('.submit-note').on('click', this.createNote.bind(this));
    }


    deleteNote(event) {
        this.deleteReachedNoteLimitAlert();

        var note = $(event.target).parents('li');

        $.ajax({
            beforeSend: (xhr) => {
                xhr.setRequestHeader('X-WP-Nonce', backendJsData.nonce);
            },
            url: backendJsData.root_url + '/wp-json/wp/v2/note/' + note.data('id'),
            type: 'DELETE',
            success: (response) => {
                note.slideUp();
                console.log('SUCCESS');
                console.log(response);
                // if (response.userNoteCount < 5) {
                //     $('.note-limit-message').text('').removeClass('active');
                // }
            },
            error: (response) => {
                console.log('ERROR');
                console.log(response);
            }
        });
    }

    updateNote(event) {
        this.deleteReachedNoteLimitAlert();
        var note = $(event.target).parents('li');
        var data = {
            title: note.find('.note-title-field').val(),
            content: note.find('.note-body-field').val()
        };
        $.ajax({
            beforeSend: (xhr) => {
                xhr.setRequestHeader('X-WP-Nonce', backendJsData.nonce);
            },
            url: backendJsData.root_url + '/wp-json/wp/v2/note/' + note.data('id'),
            type: 'POST',
            data: data,
            success: (response) => {
                console.log('UPDATE SUCCESS');
                console.log(response);
                this.makeNoteEditableOrNot(note, false);
            },
            error: (response) => {
                console.log('ERROR');
                console.log(response);
            }
        });
    }

    createNote(event) {

        this.deleteReachedNoteLimitAlert();

        var data = {
            title: $('.new-note-title').val(),
            content: $('.new-note-body').val(),

            // The default status is draft
            // status: 'publish'

            // If we want all notes to be hidden in GET /wp-json/wp/v2/note'
            // We have second (more secure) check in BE in function.php with wp_insert_post_data !!!
            status: 'private'
        };
        $.ajax({
            beforeSend: (xhr) => {
                xhr.setRequestHeader('X-WP-Nonce', backendJsData.nonce);
            },
            url: backendJsData.root_url + '/wp-json/wp/v2/note',
            type: 'POST',
            data: data,
            success: (response) => {
                console.log('CREATE SUCCESS');
                console.log(response);
                $('.new-note-title,.new-note-body').val('');
                $(`<li data-id="${response.id}">
                <input readonly class="note-title-field" value="${response.title.raw}">
                <span class="edit-note"><i class="fa fa-pencil" aria-hidden="true"> Edit</i></span>
                <span class="delete-note"><i class="fa fa-trash-o" aria-hidden="true"> Delete</i></span>
                <textarea readonly class="note-body-field">${response.content.raw}</textarea>
                <span class="update-note btn-primary"><i class="fa fa-arrow-right"
                                                         aria-hidden="true"> </i>Save</span>
            </li>`).prependTo('#my-notes').hide().slideDown();

                this.events();
            },
            error: (response) => {
                if (typeof response.responseText !== 'undefined') {
                    $('.note-limit-message').text(response.responseText).addClass('active');
                }
                console.log('ERROR');
                console.log(response);
            }
        });
    }

    editNote(event) {
        var note = $(event.target).parents('li');

        if (note.data('state') === 'editable') {
            // Make read only
            this.makeNoteEditableOrNot(note, false);
        } else {
            // Make editable
            this.makeNoteEditableOrNot(note, true);
        }

        this.deleteReachedNoteLimitAlert();
    }

    makeNoteEditableOrNot(note, isEditable) {
        if (isEditable) {
            note.find('.edit-note').html('<i class="fa fa-times" aria-hidden="true"> Cancel</i>');
            note.find('.note-title-field, .note-body-field').removeAttr('readonly').addClass('note-active-field');
            note.find('.update-note').addClass('update-note--visible');
            note.data('state', 'editable');
        } else {
            note.find('.edit-note').html('<i class="fa fa-times" aria-hidden="true"> Edit</i>');
            note.find('.note-title-field, .note-body-field').attr('readonly', 'readonly').removeClass('note-active-field');
            note.find('.update-note').removeClass('update-note--visible');
            note.data('state', 'cancel');
        }
    }


    deleteReachedNoteLimitAlert() {
        $('.note-limit-message').text('').removeClass('active');
    }


}

export default Notes;
