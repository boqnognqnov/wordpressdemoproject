import $ from 'jquery';

class Like {
    constructor() {
        this.events();
    }

    events() {
        $('.like-box').on('click', this.clickDispatcher.bind(this));
    }


    clickDispatcher(event) {
        var currentLikeBox = $(event.target).closest('.like-box');

        if (currentLikeBox.attr('data-exists') === 'yes') {
            this.deleteLike(currentLikeBox);
        } else {
            this.createLike(currentLikeBox);
        }
    }

    createLike(currentLikeBox) {
        $.ajax({
            beforeSend: (xhr) => {
                xhr.setRequestHeader('X-WP-Nonce', backendJsData.nonce);
            },
            url: backendJsData.root_url + '/wp-json/university/v1/manage/like',
            type: 'POST',
            data: {
                professorId: currentLikeBox.data('professorId')
            },
            success: (response) => {
                console.log('SUCCESS');
                console.log(response);
                currentLikeBox.attr('data-exists', 'yes');
                var previousLikeCount = parseInt(currentLikeBox.find('.like-count').html());
                previousLikeCount++;
                currentLikeBox.find('.like-count').html(previousLikeCount)
                currentLikeBox.attr('data-like-id', response['id']);
            },
            error: (response) => {
                console.log('ERROR');
                console.log(response);
            }
        });
    }


    deleteLike(currentLikeBox) {
        $.ajax({
            beforeSend: (xhr) => {
                xhr.setRequestHeader('X-WP-Nonce', backendJsData.nonce);
            },
            // url: backendJsData.root_url + '/wp-json/university/v1/manage/like/' + currentLikeBox.data('professorId'),
            url: backendJsData.root_url + '/wp-json/university/v1/manage/like',
            data: {likePostId: currentLikeBox.attr('data-like-id')},
            type: 'DELETE',
            success: (response) => {
                console.log('SUCCESS');
                console.log(response);
                currentLikeBox.attr('data-exists', 'no');
                var previousLikeCount = parseInt(currentLikeBox.find('.like-count').html());
                previousLikeCount--;
                currentLikeBox.find('.like-count').html(previousLikeCount)
                currentLikeBox.attr('data-like-id', '');
            },
            error: (response) => {
                console.log('ERROR');
                console.log(response);
            }
        });
    }
}

export default Like;