// 3rd party packages from NPM
import $ from 'jquery';
import slick from 'slick-carousel';

// Our modules / classes
import MobileMenu from './modules/MobileMenu';
import HeroSlider from './modules/HeroSlider';
import GlobalSearch from './modules/Search';
import Notes from './modules/notes';
import Like from './modules/Like';
// import GoogleMap from './modules/GoogleMap';


// Instantiate a new object using our modules/classes
var mobileMenu = new MobileMenu();
var heroSlider = new HeroSlider();
var globalSearch = new GlobalSearch();
var notes = new Notes();
var like = new Like();


// var googleMap = new GoogleMap();
