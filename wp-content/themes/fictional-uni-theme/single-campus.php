<?php
get_template_part('template-parts/googleMapScripts');
?>
<?php
get_header();
?>

<?php
while (have_posts()) {
    the_post();

    pageBanner([
        'title' => get_the_title(),
        'sub_title' => get_the_archive_description(),
    ]);

    ?>

    <div class="container container--narrow page-section">
        <h2>
            <?php the_title(); ?>
        </h2>

        <div class="metabox metabox--position-up metabox--with-home-link">
            <p>
                <a class="metabox__blog-home-link" href="<?= get_post_type_archive_link('campus'); ?>">
                    <i class="fa fa-home"
                       aria-hidden="true"></i>
                    All Campuses
                </a>
                <span class="metabox__main"><?php the_title(); ?></span>
            </p>
        </div>

        <div class="generic-content">
            <?php the_content(); ?>
        </div>

        <?php $mapLocation = get_field('map_location'); ?>
        <div class="acf-map">
            <div class="marker" data-lat="<?= $mapLocation['lat'] ?>" data-lng="<?= $mapLocation['lng'] ?>">
                <h3><b><?php the_title(); ?></b></h3>
                <?php echo $mapLocation['address']; ?>
            </div>
        </div>


        <?php

        $relatedPrograms = new WP_Query([
            'posts_per_page' => -1,
            'post_type' => 'program',
            'orderBy' => 'title',
            'order' => 'ASC',
            'meta_query' => [
                [
                    'key' => 'related_campus',
                    'compare' => 'LIKE',
                    'value' => '"' . get_the_ID() . '"'
//                    'value' => get_the_ID()
                ]
            ]
        ]);
        ?>

        <?php if ($relatedPrograms->have_posts()) { ?>

            <h2><?php echo get_the_title() . ' '; ?>Program<br></h2>

            <ul class="min-list link-list">
                <?php while ($relatedPrograms->have_posts()) {
                    $relatedPrograms->the_post();
                    ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>

        <?php } ?>

        <?php wp_reset_postdata(); ?>


        <br>
        <br>

        <div class="metabox">
            <p>Posted by <?php the_author_posts_link(); ?> on <?php the_time('d/m/y h:m'); ?>
                in <?php echo get_the_category_list(', '); ?></p>
        </div>
    </div>


<?php } ?>

<?php get_footer(); ?>


