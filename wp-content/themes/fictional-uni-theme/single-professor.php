<?php

get_header();
?>

<?php
while (have_posts()) {
    the_post(); ?>

    <?php
    pageBanner();
    ?>

    <div class="container container--narrow page-section">
        <h2>
            <?php the_title(); ?>
        </h2>


        <div class="generic-content">
            <div class="row group">
                <div class="one-third">
                    <?php the_post_thumbnail('professorPortrait'); ?>
                </div>
                <div class="two-third">
                    <?php
                    $likeCount = new WP_Query(
                        [
                            'post_type' => 'like',
                            'meta_query' => [
                                [
                                    'key' => 'liked_professor_id',
                                    'compare' => '=',
                                    'value' => get_the_ID()
                                ]
                            ],
                        ]
                    );

                    $existStatus = 'no';


                    $userAlreadyLiked = new WP_Query(
                        [
                            'author' => get_current_user_id(),
                            'post_type' => 'like',
                            'meta_query' => [
                                [
                                    'key' => 'liked_professor_id',
                                    'compare' => '=',
                                    'value' => get_the_ID()
                                ]
                            ],
                        ]
                    );

                    $currentLikePostId = 0;
                    //                    if (get_current_user_id() !== 0) {
                    if (is_user_logged_in()) {
                        if ($userAlreadyLiked->found_posts > 0) {
                            $existStatus = 'yes';
                        }

                        $currentLikePostId = $userAlreadyLiked->posts[0]->ID;
                    }


                    ?>
                    <span class="like-box" data-exists="<?= $existStatus; ?>" data-professor-id="<?php the_ID(); ?>"
                          data-like-id="<?php echo $currentLikePostId; ?>">
                        <i class="fa fa-heart-o" aria-hidden="true"></i>
                        <i class="fa fa-heart" aria-hidden="true"></i>
                        <span class="like-count"><?php echo $likeCount->found_posts ?></span>
                    </span>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>


        <?php
        $relatedPrograms = get_field('related_programs');
        if ($relatedPrograms) { ?>
            <hr class="section-break">
            <h2 class="headline headline--medium">Subject(s) Taught</h2>
            <ul class="link-list min-list">
                <?php foreach ($relatedPrograms as $program) { ?>
                    <li><a href="<?= get_the_permalink($program); ?>"><?php echo get_the_title($program); ?></a></li>
                <?php } ?>
            </ul>
        <?php } ?>
        <br>
        <br>

        <div class="metabox">
            <p>Posted by <?php the_author_posts_link(); ?> on <?php the_time('d/m/y h:m'); ?>
                in <?php echo get_the_category_list(', '); ?></p>
        </div>
    </div>


<?php } ?>

<?php get_footer(); ?>


