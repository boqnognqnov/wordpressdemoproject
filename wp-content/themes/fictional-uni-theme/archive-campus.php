<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATAPRyIDzSOB2zoMySk50SIqSaMq3eRc8"></script>-->
<?php
get_template_part('template-parts/googleMapScripts');
?>
<?php
get_header();
?>




<?php
//echo the_archive_description();
$title = 'Our Campuses';
$sub_title = 'Our Campuses description';
if (pll_current_language() == 'bg') {
    $title = 'Нашите Кампуси';
    $sub_title = 'Нашите Кампуси Заглавие';
}
pageBanner([
    'title' => $title,
    'sub_title' => $sub_title,
    'background_img_assets' => ['url' => 'our-campuses.png', 'default' => true],
]);
?>

<div class="container container--narrow page-section">
    <div class="acf-map">
        <!--        <ul class="link-list min-list">-->
        <?php while (have_posts()) {
            the_post();
            $mapLocation = get_field('map_location');
            ?>
            <div class="marker" data-lat="<?= $mapLocation['lat'] ?>" data-lng="<?= $mapLocation['lng'] ?>">
                <h3><b><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></b></h3>
                <?php echo $mapLocation['address']; ?>
            </div>
            <li><a href="<?php the_permalink(); ?>">
                    <?php
                    the_title();
                    ?>
                </a></li>
        <?php } ?>
        <!--        </ul>-->
    </div>
</div>


<?php
get_footer();
?>


