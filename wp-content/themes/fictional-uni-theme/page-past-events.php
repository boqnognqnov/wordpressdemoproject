<?php
get_header();
?>



<?php
//echo the_archive_description();
pageBanner([
    'title' => 'Past events',
    'sub_title' => get_the_archive_description(),
]);
?>

<div class="container container--narrow page-section">

    <?php
    //            $today = date('d/m/Y');
    $today = date('Ymd');
    $events = new WP_Query([
//        'paged' => get_query_var('paged', 1),
        'posts_per_page' => 2,
        'post_type' => 'event',
//                'orderBy' => 'post_date',
        'meta_key' => 'event_date',
        'orderBy' => 'meta_value_num',
        'order' => 'DESC',
        'meta_query' => [
            [
                'key' => 'event_date',
                'compare' => '<',
                'value' => $today,
                'type' => 'numeric',
            ]
        ]
    ]);
    ?>

    <?php while ($events->have_posts()) {
        $events->the_post();

        get_template_part('template-parts/content','event');
    } ?>


    <?php echo paginate_links([
        'total' => $events->max_num_pages
    ]); ?>
</div>


<?php
get_footer();
?>
