<?php

get_header();
?>


<?php
while (have_posts()) {
    the_post();
    ?>
    <?php
    pageBanner();
    ?>

    <div class="container container--narrow page-section">


        <?php
        $parentPageId = wp_get_post_parent_id(get_the_ID());
        if (wp_get_post_parent_id(get_the_ID())) { ?>

            <div class="metabox metabox--position-up metabox--with-home-link">
                <p><a class="metabox__blog-home-link" href="<?= get_permalink($parentPageId); ?>"><i class="fa fa-home"
                                                                                                     aria-hidden="true"></i>
                        Back to
                        <?= get_the_title($parentPageId); ?>
                        Us</a> <span class="metabox__main"><?php the_title(); ?></span></p>
            </div>

        <?php } ?>

        <?php
        $pagesArray = get_pages([
            'child_of' => get_the_ID()
        ]);

        if ($parentPageId !== 0 || count($pagesArray) > 0) { ?>
            <div class="page-links">
                <h2 class="page-links__title"><a
                            href="<?= get_permalink($parentPageId) ?>"><?= get_the_title($parentPageId) ?></a></h2>
                <ul class="min-list">
                    <?php

                    $findChildrenOf = get_the_ID();
                    if ($parentPageId > 0) {
                        $findChildrenOf = $parentPageId;
                    }
                    wp_list_pages([
                        'title_li' => null,
                        'child_of' => $findChildrenOf
                    ]);
                    ?>
                </ul>
            </div>

        <?php } ?>


        <div class="generic-content">
            <?php the_content(); ?>
        </div>

    </div>


    <?php
//    the_content();
}

?>


<?php

get_footer();
?>



