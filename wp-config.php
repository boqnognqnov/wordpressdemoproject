<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'data_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'secret' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '^1A*!- n-a=fi(q0da*6e6yKlKF=J=e%B6U3xmo-S&ue?{(iG+oWgYy4Ag~jMI=e' );
define( 'SECURE_AUTH_KEY',  '3(Qvk`<XK;SIp,L><@%}&^hKu$hFG$3j^gRYESS/ ?a*Sp%41_m]G.7:U(=KAiHJ' );
define( 'LOGGED_IN_KEY',    'F}pabkOtB7*typ-%X1^vph,1n|1Z&-):P+3t4%*H:@0S7$ ]3y+*x(PnL;kmzYS[' );
define( 'NONCE_KEY',        'KtV_U[wQ28nw|AFY5r>OZMF=G4])/_l{>@w@gvZ1*ABuNsgH{d@7h`(N,%Z;mu .' );
define( 'AUTH_SALT',        '>qB=36d-Rau[+>7Z*g=Yu<(?A1$R7m Idu.@>Q@ZDfCD;>Q;FWmJWTnpm]Hs&59)' );
define( 'SECURE_AUTH_SALT', '(.ywrZVal-%}.-AUDau 4uPL;0`^vR!P06ze:C@&m/O<%Ox2.Z|SoLP&1qZ67-r4' );
define( 'LOGGED_IN_SALT',   'I3=rz~%0f~[hK(cS<x.dS)G/]lCnO_~7N&r]v-+U2?;9hkM!/*Nh%Y9S>15{{2`d' );
define( 'NONCE_SALT',       '7BeKDh--LQ[LI)#4mK^z]K63BbDf*:amq2Mq106c]<tO$+qT|l>%0&vS J0_T-v ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
